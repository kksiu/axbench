#!/bin/bash

benchmark=blackscholes

for f in test.data/input/*.data
do
	filename=$(basename "$f")
	extension="${filename##*.}"
	filename="${filename%.*}"

	START=$(date +%s.%N)
	./bin/${benchmark}.nn.out ${f} test.data/output/${filename}_${benchmark}_orig.data
	DIFF=$(echo "$END - $START" | bc)
	echo "NN: $DIFF"

	START=$(date +%s.%N)
	./bin/${benchmark}.out ${f} test.data/output/${filename}_${benchmark}_nn.data
	END=$(date +%s.%N)
	DIFF=$(echo "$END - $START" | bc)
	echo "ORIG: $DIFF"

	python ./scripts/qos.py test.data/output/${filename}_${benchmark}_orig.data test.data/output/${filename}_${benchmark}_nn.data

done