#!/bin/bash

benchmark=fft

red='\033[0;31m'
blue='\033[0;34m'
green='\033[0;32m'
nc='\033[0m' # No Color


START=$(date +%s.%N)
./bin/${benchmark}.nn.out 	32768 test.data/output/${benchmark}_nn.data
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "NN: $DIFF"

START=$(date +%s.%N)
./bin/${benchmark}.out 		32768 test.data/output/${benchmark}_orig.data
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ORIG: $DIFF"

python ./scripts/qos.py test.data/output/${benchmark}_orig.data test.data/output/${benchmark}_nn.data 
