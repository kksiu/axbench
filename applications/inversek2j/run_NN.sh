#!/bin/bash

benchmark=inversek2j

red='\033[0;31m'
blue='\033[0;34m'
green='\033[0;32m'
nc='\033[0m' # No Color

for f in test.data/input/*.data
do
	filename=$(basename "$f")
	extension="${filename##*.}"
	filename="${filename%.*}"

	START=$(date +%s.%N)
	./bin/${benchmark}.nn.out 	${f} test.data/output/${filename}_${benchmark}_nn.data
	END=$(date +%s.%N)
	DIFF=$(echo "$END - $START" | bc)
	echo "NN: $DIFF"

	START=$(date +%s.%N)
	./bin/${benchmark}.out 		${f} test.data/output/${filename}_${benchmark}_orig.data
	END=$(date +%s.%N)
	DIFF=$(echo "$END - $START" | bc)
	echo "ORIG: $DIFF"
	
	echo -ne "${red}- ${f}${nc} "

	python ./scripts/qos.py test.data/output/${filename}_${benchmark}_orig.data test.data/output/${filename}_${benchmark}_nn.data 
done