#!/bin/bash
cd applications/blackscholes/

START=$(date +%s.%N)
./bin/blackscholes.out test.data/input/blackscholesTest_200K.data test.data/output/blackscholesTest_200K_blackscholes_orig.data
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "Blackscholes: $DIFF"

cd ../../applications/fft/

START=$(date +%s.%N)
./bin/fft.out 32768 test.data/output/fft_orig.data
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "FFT: $DIFF"

cd ../../applications/inversek2j/

START=$(date +%s.%N)
./bin/inversek2j.out test.data/input/theta_1000K.data test.data/output/theta_1000K_inversek2j_orig.data
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "Inversek2j: $DIFF"

cd ../../applications/jmeint/

START=$(date +%s.%N)
./bin/jmeint.out test.data/input/jmeint_1000K.data test.data/output/jmeint_1000K_jmeint_orig.data
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "jmeint: $DIFF"

cd ../../applications/jpeg/

START=$(date +%s.%N)
./bin/jpeg.out test.data/input/36.rgb test.data/output/peppers_orig.jpg
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "jpeg: $DIFF"

cd ../../applications/kmeans/

START=$(date +%s.%N)
./bin/kmeans.out test.data/input/36.rgb test.data/output/hello_orig.rgb
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "kmeans: $DIFF"

cd ../../applications/sobel/

START=$(date +%s.%N)
./bin/sobel.out test.data/input/36.rgb test.data/output/yes_sobel.rgb
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "sobel: $DIFF"
